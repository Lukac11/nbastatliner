package com.example.nbastatliner;

public class Player {
    String name;
    double points;
    double rebounds;
    double assists;
    double steals;
    double fgpct;
    double threepct;
    int age;
    String club;

    public Player(String Name, double Points, double Rebounds, double Assists, double Steals, double FgPct, double Threepct, int Age, String Club){
        this.name=Name;
        this.points=Points;
        this.rebounds=Rebounds;
        this.assists=Assists;
        this.steals=Steals;
        this.fgpct=FgPct;
        this.threepct=Threepct;
        this.age=Age;
        this.club=Club;
    }

    public String getName() {
        return name;
    }

    public double getPoints() {
        return points;
    }

    public double getAssists() {
        return assists;
    }

    public double getRebounds() {
        return rebounds;
    }

    public double getSteals() {
        return steals;
    }

    public double getFgpct() {
        return fgpct;
    }

    public double getThreepct() {
        return threepct;
    }

    public int getAge() {
        return age;
    }

    public String getClub() {
        return club;
    }
}
