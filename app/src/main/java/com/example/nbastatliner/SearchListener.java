package com.example.nbastatliner;

import android.os.Bundle;

public interface SearchListener {
    public void onSearchClick(Bundle txt);
}
