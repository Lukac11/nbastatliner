package com.example.nbastatliner;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewPageAdapter extends FragmentStatePagerAdapter {
    private static int NUM_PAGES=2;
    private List<String> tabTitles=new ArrayList<>();
    private String txt;

    public ViewPageAdapter(@NonNull FragmentManager fm){
        super(fm);

    }
    public void setTxt(String ulaz){
        this.txt=ulaz;
    }
    public Fragment getItem(int position){
        switch(position){
            case 0:
                return Input.newInstance();
            case 1:
                return History.newInstance(txt);
            default:
                return Input.newInstance();
        }
    }
    public int getCount(){
        return NUM_PAGES;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        addTitles();
        return tabTitles.get(position);
    }
    public void addTitles(){
        tabTitles.add("Search");
        tabTitles.add("History");
    }

}
