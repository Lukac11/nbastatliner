package com.example.nbastatliner;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity implements SearchListener{
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private ViewPageAdapter viewPageAdapter;
    private FeedReaderDbHelper dbHelper=new FeedReaderDbHelper(this);



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        setUpPager();
        dbHelper.getInstance(this);
        FillDatabase();



    }
    private void initViews() {
        mViewPager = findViewById(R.id.viewPager);
        mTabLayout = findViewById(R.id.tab);
    }
    private void setUpPager() {
        viewPageAdapter=new ViewPageAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(viewPageAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }
    public void FillDatabase(){
        Player LeBron=new Player("Lebron James", 27.0,7.4,7.4, 1.9, 50.4, 34.6, 35, "La Lakers");
        Player Zubac=new Player("Ivica Zubac", 7.4,5.6,0.9,0.2,57.3,0.0,23,"La Clippers");
        Player Babo=new Player("Bojan Bogdanovic", 15.7, 5.3, 3.7, 0.9, 42.4,39.1,31, "Utah Jazz");
        Player Saric=new Player("Dario Saric", 12.2, 6.2, 2.0, 1.2, 45.0, 35.7, 26, "Phoenix Suns");
        Player Harden= new Player("James Harden",34.2,8.1,9.6,2.3,41.4,38.4,29,"Brooklyn Nets");
        Player davis=new Player("Anthony Davis", 29.2,11.3,5.4,2.2,56.7,29.2,27,"La Lakers");
        Player rondo=new Player("Rajon Rondo", 5.7,5.6,8.9,2.2,43.5,34.4,33,"Atlanta Hawks");
        Player luka=new Player("Luka Doncic", 31.2,9.2,11.5,3.1,39.2,33.3,23,"Dallas Mavericks");
        dbHelper.insertPlayer(LeBron);
        dbHelper.insertPlayer(Zubac);
        dbHelper.insertPlayer(Babo);
        dbHelper.insertPlayer(Saric);
        dbHelper.insertPlayer(Harden);
        dbHelper.insertPlayer(davis);
        dbHelper.insertPlayer(rondo);
        dbHelper.insertPlayer(luka);
    }



    @Override
    public void onSearchClick(Bundle input) {
        String txt=input.getString("search");
        viewPageAdapter.setTxt(txt);


    }
}
