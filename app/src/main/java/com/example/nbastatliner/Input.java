package com.example.nbastatliner;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes;


public class Input extends Fragment  {
    private  static SQLiteDatabase db;
    private  Button searchButton;
    private  EditText etSearch;
    private  TextView tvPlayerName;
    private  TextView tvPoints;
    private  TextView tvRebs;
    private  TextView tvAssist;
    private  TextView tvSteals;
    private  TextView tvFgPct;
    private  TextView tvthreepct;
    private  TextView tvAge;
    private  TextView tvClub;
    private RecyclerAdapter adapter=new RecyclerAdapter((NameClickListener) getActivity());
    //private History history=new History();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_input, container, false);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InitUI(view);



    }

    public static Input newInstance(){
        Input fragment=new Input();

        return fragment;
    }
    private void InitUI(View view){
        searchButton=view.findViewById(R.id.btnAdd);
        etSearch=view.findViewById(R.id.etName);
        tvPlayerName=view.findViewById(R.id.tvIme);
        tvPoints=view.findViewById(R.id.tvPoeni);
        tvRebs=view.findViewById(R.id.tvRebs);
        tvAssist=view.findViewById(R.id.tvAssist);
        tvSteals=view.findViewById(R.id.tvSteals);
        tvFgPct=view.findViewById(R.id.tvFgPct);
        tvthreepct=view.findViewById(R.id.tv3pt);
        tvAge=view.findViewById(R.id.tvAge);
        tvClub=view.findViewById(R.id.tvClub);


        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonClick();
                /*Bundle args = new Bundle();
                Intent intent=new Intent();
                args.putString("input",etSearch.getText().toString());
                intent.putExtras(args);
                getActivity().startActivity(intent);*/

            }
        });
    }





    public void onButtonClick() {
            if(etSearch.getText().toString().trim().length()==0){
                Toast.makeText(getActivity(),"Please enter player name", Toast.LENGTH_SHORT).show();
            }else{
                Search(getActivity(),etSearch.getText().toString());

                History.getInstance().addCell(etSearch.getText().toString());
            }






    }
    public  void Search(Context context, String searchText){
        FeedReaderDbHelper dbHelper=new FeedReaderDbHelper(context);
        db=dbHelper.getWritableDatabase();
        String sql="SELECT * FROM "+FeedReaderContract.FeedEntry.table_name+" WHERE " +
                FeedReaderContract.FeedEntry.column_name + " LIKE '" + searchText+ "%'";
        Cursor cursor=db.rawQuery(sql,null);
        if(cursor.getCount()>0) {
            if (cursor.moveToFirst()) {
                do {
                    String playerName = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.column_name));
                    double points = cursor.getDouble(cursor.getColumnIndex(FeedReaderContract.FeedEntry.column_name2));
                    double rebounds = cursor.getDouble(cursor.getColumnIndex(FeedReaderContract.FeedEntry.column_name3));
                    double assists = cursor.getDouble(cursor.getColumnIndex(FeedReaderContract.FeedEntry.column_name4));
                    double steals = cursor.getDouble(cursor.getColumnIndex(FeedReaderContract.FeedEntry.column_name5));
                    double fgpct = cursor.getDouble(cursor.getColumnIndex(FeedReaderContract.FeedEntry.column_name6));
                    double threepct = cursor.getDouble(cursor.getColumnIndex(FeedReaderContract.FeedEntry.column_name7));
                    int age = cursor.getInt(cursor.getColumnIndex(FeedReaderContract.FeedEntry.column_name8));
                    String club = cursor.getString(cursor.getColumnIndex(FeedReaderContract.FeedEntry.column_name9));
                    tvPlayerName.setText("PLAYER NAME: " + playerName);
                    tvPoints.setText("Pts: " + points);
                    tvRebs.setText("Reb: " + rebounds);
                    tvAssist.setText("Ast: " + assists);
                    tvSteals.setText("St: " + steals);
                    tvFgPct.setText("FG% " + fgpct);
                    tvthreepct.setText("3PT%: " + threepct);
                    tvAge.setText("Age: " + age);
                    tvClub.setText("Club: " + club);
                } while (cursor.moveToNext());
            }
        }else{
                Toast.makeText(context, "No data was found in the system!", Toast.LENGTH_LONG).show();
            }
            cursor.close();
        }


    }







