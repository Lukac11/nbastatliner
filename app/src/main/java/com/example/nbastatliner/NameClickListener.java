package com.example.nbastatliner;

public interface NameClickListener {

    void onButtonClick(int position);
}
