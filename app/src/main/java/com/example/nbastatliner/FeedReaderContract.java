package com.example.nbastatliner;

import android.provider.BaseColumns;

public final class FeedReaderContract {
    private FeedReaderContract(){}
    public static class FeedEntry implements BaseColumns{
        public static final String table_name="NBAStatliner";
        public static final String column_name="PlayerName";
        public static final String column_name2="Points";
        public static final String column_name3="Rebounds";
        public static final String column_name4="Assists";
        public static final String column_name5="Steals";
        public static final String column_name6="fgpct";
        public static final String column_name7="threepct";
        public static final String column_name8="age";
        public static final String column_name9="club";
    }

}
