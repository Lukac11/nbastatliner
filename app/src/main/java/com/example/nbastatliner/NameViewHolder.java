package com.example.nbastatliner;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NameViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    private TextView tvName;
    private NameClickListener clickListener;
    private ImageButton removebtn;

    public NameViewHolder(@NonNull View itmView, NameClickListener listener){
        super(itmView);
        this.clickListener=listener;
        tvName=itmView.findViewById(R.id.tvName);
        removebtn=itmView.findViewById(R.id.btnRemove);
        removebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onButtonClick(getAdapterPosition());
            }
        });
        itmView.setOnClickListener(this);
    }
    public void setName(String name){
        tvName.setText(name);
    }
    public void onClick(View view){
        clickListener.onButtonClick(getAdapterPosition());
    }

}
