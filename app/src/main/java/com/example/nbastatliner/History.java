package com.example.nbastatliner;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class History extends Fragment implements NameClickListener {

    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;
    private static History instance;


    public  static History newInstance(String ulaz){

        History fragment=new History();


        instance=fragment;
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupRecycler(view);
        if (getArguments() != null) {
            String one = getArguments().getString("input");
            adapter.addNewCell(one,0);
        }
    }
    private void setupRecycler(View view){

        recyclerView=view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter=new RecyclerAdapter(this);
        recyclerView.setAdapter(adapter);

    }
    public void addCell(String txt){

        adapter.addNewCell(txt,0);

    }


    @Override
    public void onButtonClick(int position) {
        adapter.removeCell(position);
    }
    public static History getInstance(){
        return instance;
    }
}