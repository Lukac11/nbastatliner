package com.example.nbastatliner;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.nbastatliner.FeedReaderContract.*;

import static com.example.nbastatliner.FeedReaderContract.FeedEntry.table_name;
import static okhttp3.internal.Internal.instance;

public class FeedReaderDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "NBAStatliner.db";
    private static FeedReaderDbHelper instance;
    private  SQLiteDatabase db;

    private static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + table_name + "(" +
                        FeedReaderContract.FeedEntry.column_name + " TEXT," +
                        FeedReaderContract.FeedEntry.column_name2 + " DOUBLE," +
                        FeedReaderContract.FeedEntry.column_name3 + " DOUBLE," +
                        FeedReaderContract.FeedEntry.column_name4 + " DOUBLE," +
                        FeedReaderContract.FeedEntry.column_name5 + " DOUBLE," +
                        FeedReaderContract.FeedEntry.column_name6 + " DOUBLE," +
                        FeedReaderContract.FeedEntry.column_name7 + " DOUBLE," +
                        FeedReaderContract.FeedEntry.column_name8 + " INTEGER," +
                        FeedReaderContract.FeedEntry.column_name9 + " TEXT)";
    private static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + table_name;



    public FeedReaderDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public static synchronized FeedReaderDbHelper getInstance(Context context) {
        if (instance == null) {
            instance = new FeedReaderDbHelper(context.getApplicationContext());
        }
        return instance;
    }



    public void onCreate(SQLiteDatabase DB) {
        this.db=DB;
        //db.execSQL(SQL_CREATE_ENTRIES);
        db.execSQL(SQL_CREATE_ENTRIES);
        //FillDatabase();
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
        //FillDatabase();
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
    public void insertPlayer (Player player) {
        db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FeedEntry.column_name, player.getName());
        contentValues.put(FeedEntry.column_name2, player.getPoints());
        contentValues.put(FeedEntry.column_name3, player.getRebounds());
        contentValues.put(FeedEntry.column_name4, player.getAssists());
        contentValues.put(FeedEntry.column_name5, player.getSteals());
        contentValues.put(FeedEntry.column_name6, player.getFgpct());
        contentValues.put(FeedEntry.column_name7, player.getThreepct());
        contentValues.put(FeedEntry.column_name8, player.getAge());
        contentValues.put(FeedEntry.column_name9, player.getClub());
        db.insert(FeedEntry.table_name, null, contentValues);
    }
    /*public void FillDatabase(){
        Player LeBron=new Player("Lebron James", 27.0,7.4,7.4, 1.9, 50.4, 34.6, 35, "La Lakers");
        Player Zubac=new Player("Ivica Zubac", 7.4,5.6,0.9,0.2,57.3,0.0,23,"La Clippers");
        Player Babo=new Player("Bojan Bogdanovic", 15.7, 5.3, 3.7, 0.9, 42.4,39.1,31, "Utah Jazz");
        Player Saric=new Player("Dario Saric", 12.2, 6.2, 2.0, 1.2, 45.0, 35.7, 26, "Phoenix Suns");
        Player Harden= new Player("James Harden",34.2,8.1,9.6,2.3,41.4,38.4,29,"Brooklyn Nets");
        Player davis=new Player("Anthony Davis", 29.2,11.3,5.4,2.2,56.7,29.2,27,"La Lakers");
        Player rondo=new Player("Rajon Rondo", 5.7,5.6,8.9,2.2,43.5,34.4,33,"Atlanta Hawks");
        Player luka=new Player("Luka Doncic", 31.2,9.2,11.5,3.1,39.2,33.3,23,"Dallas Mavericks");
        insertPlayer(LeBron);
        insertPlayer(Zubac);
        insertPlayer(Babo);
        insertPlayer(Saric);
        insertPlayer(Harden);
        insertPlayer(davis);
        insertPlayer(rondo);
        insertPlayer(luka);
    }*/


}
